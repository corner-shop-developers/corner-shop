from django import forms
from .models import ShippingAddress

class ShippingForm(forms.ModelForm):
	full_name = forms.CharField(widget=forms.TextInput(attrs={'class':'input', 'placeholder':'Full Name'}) , required=True)
	email = forms.EmailField(widget=forms.TextInput(attrs={'class':'input',  'placeholder':'Email Address'}), required=True)
	address1 = forms.CharField(widget=forms.TextInput(attrs={'class':'input',  'placeholder':'Address1'}), required=True)
	city = forms.CharField(widget=forms.TextInput(attrs={'class':'input',  'placeholder':'City'}), required=True)
	country = forms.CharField(widget=forms.TextInput(attrs={'class':'input',  'placeholder':'Country'}), required=True)

	class Meta():
		model = ShippingAddress
		fields = ['full_name', 'email', 'address1', 'city', 'country']
		exclude =[ 'user', ]