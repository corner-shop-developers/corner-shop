from django.shortcuts import render
from cart.cart import Cart
from payment.forms import ShippingForm
from .models import  ShippingAddress

def billing(request):
        #Get the cart
        cart = Cart(request)
        cart_products = cart.get_products
        quantities = cart.get_quantities
        totals = cart.cart_total()


        shipping_form = request.POST
        return render(request, 'payment/billing.html', {'cart_products':cart_products, "quantities":quantities, 'totals':totals,  'shipping_form':shipping_form })

def checkout(request):
     #Get the cart
    cart = Cart(request)
    cart_products = cart.get_products
    quantities = cart.get_quantities
    totals = cart.cart_total()

    if request.user.is_authenticated:
        #Checkout as logged in user
         shipping_user = ShippingAddress.objects.get(user__id=request.user.id)
         shipping_form = ShippingForm(request.POST or None, instance=shipping_user)
         return render(request, 'payment/checkout.html', {'cart_products':cart_products, "quantities":quantities, 'totals':totals,  'shipping_form':shipping_form })
    else:
        #Checkout as guest.
        return render(request, 'payment/checkout.html', {'cart_products':cart_products, "quantities":quantities, 'totals':totals,  'shipping_form':shipping_form })

def payment_sucess(request):
    return render(request, 'payment/payment.html', {})
