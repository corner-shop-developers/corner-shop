from django.shortcuts import render, redirect
from .models import Product, BusinessRegistration
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms
from .forms import SignupForm, BusinessRegistrationForm, UpdateUserForm, ChangePasswordForm, ProductForm, ContactForm
from payment.forms import ShippingForm
from payment.models import ShippingAddress
from django.contrib.auth.decorators import login_required
from django.db.models import Q
#where Q stands for complex query.


# Create your views here.
def home(request):
    products = Product.objects.all()
    return render(request,'conerapp/index.html',{'products':products})

def shop(request):
    products = Product.objects.all()
    return render(request, 'conerapp/shop.html',{'products':products} )

def searchbar(request):
    if request.method == 'GET':
        search = request.GET.get('search')
        if search:
            products = Product.objects.filter(
            Q(name__icontains=search) | 
            Q(category__name__icontains=search) |
            Q(price__icontains=search)
            )
            # products = Product.objects.filter(name__icontains=search)
            # products = Product.objects.filter(price__icontains=search)
            # products = Product.objects.filter(category__icontains=search)
            # multiple_search = Q(Q(name__icontains=search) | Q(price__icontains=search) | Q(category__icontains=search))
            # products = Product.objects.filter(multiple_search)
            return render(request, 'conerapp/searchbar.html', {'products':products})
        else:
            print('No Products Found')
            return render(request, 'conerapp/searchbar.html', { })

def addproduct(request):
    form = ProductForm()

    if request.method == 'POST': 
        form = ProductForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        form = ProductForm()

    context = {
        'form':form
    }
    return render(request, 'conerapp/addproduct.html', context)


        
# @login_required(login_url ='login')
def product_details(request, pk):
    product = Product.objects.get(id=pk)
    return render(request, 'conerapp/product.html',  {'product':product} )


def contacts(request):
    if request.method=="POST":
            form=ContactForm(request.POST)
            if form.is_valid():
                form.save()
                
                messages.success(request, ('Message has been successfully sent!!!!'))
                return redirect('contacts')
            
            else:
                 messages.success(request, ('Message failed !!!!'))
                 return redirect('contacts')
    else:
        form=ContactForm()
        return render(request, 'conerapp/contacts.html',{'form':form})      
def about(request):
    return render(request,'conerapp/about.html')

def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, ('You have been logged in!!!!'))
            return redirect('/')
        else:
            messages.success(request, ('There was an error please try again---'))
            return redirect('login')
    else:
        return render(request,'conerapp/login.html')

def logout_user(request):
    logout(request)
    messages.success(request, ('You have been logged out successfully!!!!!!!'))
    return redirect('home')

def register_user(request):
        if request.method=="POST":
            form=SignupForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, 'Successfully registered login for verification!!!!')
                return redirect('login')
        else:
            form=SignupForm()
            messages.success(request, 'An error occured Try again!!!!')
            return render(request, 'conerapp/register.html',{'form':form})
    

def business_register(request):
    if request.method =="POST":
        forma = BusinessRegistrationForm(request.POST)
        if forma.is_valid():
            forma.save()
            return redirect('shop')
    else:
        forma = BusinessRegistrationForm()
        return render(request, 'conerapp/business.html', {'forma':forma})
    
@login_required(login_url ='login')    
def update_user(request):
    if request.user.is_authenticated:
        current_user = User.objects.get(id=request.user.id)
        shipping_user = ShippingAddress.objects.get(user__id=request.user.id)

        user_form = UpdateUserForm(request.POST or None, instance=current_user)
        shipping_form = ShippingForm(request.POST or None, instance=shipping_user)

        if user_form.is_valid() or shipping_form.is_valid():
            user_form.save()
            shipping_form.save()

            login(request, current_user, shipping_user)
            messages.success(request, 'Your Profile has been sucessfully udated!!!!')
            return redirect('home')
        return render(request, 'conerapp/update_user.html', {'user_form':user_form,  'shipping_form':shipping_form})

    else:
        messages.success(request, 'Try again!!!!')
        return redirect('home')
    

def update_password(request):
    if request.user.is_authenticated:
      current_user = request.user

      if request.method == "POST":
          form = ChangePasswordForm(current_user, request.POST)
          if form.is_valid():
              form.save()
              messages.success(request, 'your password has been update!!!')

          else:
              for error in list(form.errors.values()):
                  messages.error(request,error)
                  return redirect('update_password')

    

      else:
        form = ChangePasswordForm(current_user)
        form.fields['new_password1'].help_text = ""

        return render(request, 'conerapp/update_password.html',{'form':form})
    
    else:
        messages.success(request, 'You must be logged in!!!!')
        return redirect('home')
