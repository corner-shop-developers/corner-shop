
from django.urls import path
from .import views

urlpatterns = [
    
    path('', views.home,name ='home'),
    path('contacts/', views.contacts,name ='contacts'),
    path('about/', views.about,name ='about'),
    path('shop/', views.shop, name ='shop'),
    path('login/', views.login_user,name ='login'),
    path('logout/', views.logout_user,name ='logout'),
    path('update_user/', views.update_user,name ='update_user'),
    path('update_password/', views.update_password,name ='update_password'),
    path('register/', views.register_user,name ='register'),
    path('business/', views.business_register, name ='business'),
    path('search/', views.searchbar, name ='search'),
    path('addproduct/', views.addproduct,name ='addproduct'),
    path('product/<int:pk>', views.product_details, name ='product_details'),
    ]