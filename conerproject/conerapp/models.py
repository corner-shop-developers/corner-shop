from django.db import models
import datetime

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=25)
    class Meta:
        verbose_name_plural='Categories'
    
    def __str__(self):
        return self.name

class Product(models.Model):
    category = models.ForeignKey(Category,on_delete= models.CASCADE)
    name = models.CharField(max_length=25)
    description = models.TextField(default = '' ,blank=True, null=True)
    price = models.IntegerField()
    image = models.ImageField(upload_to='uploads/product/')
    is_sold = models.BooleanField(default=False)
    def __str__(self):
        return self.name
    
class Customer(models.Model):
    firstname = models.CharField(max_length=25)
    lastname = models.CharField(max_length=25)
    email = models.EmailField()
    contact = models.CharField(max_length=30)
    def __str__ (self):
        return f'{self.firstname} {self.lastname}'
    

class Order(models.Model):
    customer = models.ForeignKey(Customer,on_delete=models.CASCADE)
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    date = models.DateTimeField(default = datetime . datetime.today)
    quantity = models.IntegerField(default = 0)

class BusinessRegistration(models.Model):
    firstname = models.CharField(max_length=25)
    lastname = models.CharField(max_length=25)
    email = models.EmailField()
    contact = models.CharField(max_length=25)
    business_name = models.CharField(max_length=30)
    bussiness_registration_number = models.CharField(max_length=25)
    address = models.CharField(max_length=30)
    descrption = models.TextField()
    
    def __str__(self):
        return f'{self.business_name} {self.address}'
    


class Contact(models.Model):
    Name=models.CharField(max_length=255, null=False)
    Email=models.EmailField()
    Message=models.TextField()

    def __str__(self):
        return self.Name   
    