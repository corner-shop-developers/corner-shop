from conerapp.models import Product
class Cart():
        def __init__(self, request):
                self.session = request.session

                #getting the session key if it exists.
                cart = self.session.get('session_key')

                #If user is new and has no session key, create one !.
                if 'session_key' not in request.session:

                    cart = self.session['session_key'] = {}

                #This helps to make sure that cart is avalable on all pages!.
                self.cart = cart

        def add(self, product, quantity):

                product_id = str(product.id)
                product_qty = str(quantity)
                if product_id in self.cart:
                        pass
                else:
                #self.cart[ product_id ] ={'Price ': str(product.price)}
                    self.cart[ product_id ] =int(product_qty)
                self.session.modified = True

        def __len__(self):
                return len(self.cart)

        def get_products(self):
            #Getting ids from cart
            product_ids = self.cart.keys()
            #Use the ids to look up products in the DB model
            products = Product.objects.filter( id__in = product_ids)

            return products

        def get_quantities(self):
            quantities = self.cart
            return quantities

        def update(self, product, quantity):
            product_id = str(product)
            product_qty = int(quantity)

            #Get our cart
            ourcart = self.cart
            #Update ur cart/dictionary
            ourcart[product_id] = product_qty

            self.session.modified = True

            updatedcart = self.cart
            return updatedcart

        def delete(self, product):
            product_id = str(product)
            #Delete from Cart dictionary
            if product_id in self.cart:  #Checking if our product is in cart
                del self.cart[product_id]

            self.session.modified = True

        def cart_total(self):
            #Get product ids
            product_ids = self.cart.keys()
            #Look for the keys in our product database model
            products = Product.objects.filter(id__in=product_ids)
            #Get the quantities
            quantities = self.cart

            total = 0
            for key, value in quantities.items():
                #Convert key into an integer for easy math calculations
                key = int(key)
                for product in products:
                    if product.id == key:
                            total = total + (product.price * value)

            return total
