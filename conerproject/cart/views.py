from django.shortcuts import render, get_object_or_404
from .cart import Cart
from conerapp.models import Product
from django.http import JsonResponse
from django.contrib import messages


# Create your views here.
def cart_summary(request):
    #Get the cart
    cart = Cart(request)
    cart_products = cart.get_products
    quantities = cart.get_quantities
    totals = cart.cart_total()

    return render(request, 'cart_summary.html', {'cart_products':cart_products, "quantities":quantities, 'totals':totals})

def cart_add(request):
    cart = Cart(request)
    # Testing for post
    if request.POST.get('action') == 'post':
        product_id = int(request.POST.get('product_id'))
        product_qty = int(request.POST.get('product_qty'))

		#look up the product in he database
        product = get_object_or_404(Product, id = product_id)

		#save to the session
        cart.add(product=product, quantity=product_qty)

		#Get Quantity
        cart_quantity = cart.__len__()

        responce = JsonResponse({'qty :' : cart_quantity})
        messages.success(request, ('Product has been Added.'))
        #responce = JsonResponse({'Product Name :' : product.name})
        return responce


def cart_delete(request):
    cart = Cart(request)
    if request.POST.get('action') == 'post':
        product_id = int(request.POST.get('product_id'))

        #Call our delete functon
        cart.delete(product=product_id)
        responce = JsonResponse({'product':product_id})
        messages.success(request, ('Product successfully Deleted...'))
        return responce
        #return redirect('cart_summary')


def cart_update(request):
    cart = Cart(request)
    if request.POST.get('action') == 'post':
        product_id = int(request.POST.get('product_id'))
        product_qty = int(request.POST.get('product_qty'))

        cart.update(product=product_id, quantity=product_qty)
        responce = JsonResponse({'qty':product_qty})
        messages.success(request, ('Your Cart has been  Updated...'))
        return responce
        #return redirect('cart_summary')
