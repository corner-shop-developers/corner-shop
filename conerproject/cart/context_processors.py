from .cart import Cart

#This context processor helps our cart to work on all pages

def cart(request):
    
        #Returning our default data from the our cart
        return {'cart': Cart(request)}
