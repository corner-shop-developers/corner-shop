# from django.db import models
# from django.contrib.auth.models import User

# class Category(models.Model):
#     name = models.CharField(max_length=25)

#     class Meta:
#         verbose_name_plural = 'Categories'
    
#     def __str__(self):
#         return self.name


# class Customer(models.Model):
#     firstname = models.CharField(max_length=25)
#     lastname = models.CharField(max_length=25)
#     email = models.EmailField()
#     contact = models.CharField(max_length=30)

#     def __str__(self):
#         return f'{self.firstname} {self.lastname}'


# class Product(models.Model):
#     category = models.ForeignKey(Category, on_delete=models.CASCADE)
#     name = models.CharField(max_length=25)
#     description = models.TextField(default='', blank=True, null=True)
#     price = models.IntegerField()
#     image = models.ImageField(upload_to='uploads/product/')
#     is_sold = models.BooleanField(default=False)

#     def __str__(self):
#         return self.name

#     @property
#     def imageURL(self):
#         try:
#             url = self.image.url
#         except:
#             url = ''
#         return url


# class Order(models.Model):
#     customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True, blank=True)
#     date_ordered = models.DateTimeField(auto_now_add=True)
#     complete = models.BooleanField(default=False)
#     transaction_id = models.CharField(max_length=100, null=True)

#     def __str__(self):
#         return str(self.id)
        
#     @property
#     def shipping(self):
#         shipping = False
#         orderitems = self.orderitem_set.all()
#         for i in orderitems:
#             if not i.product.digital:
#                 shipping = True
#                 break
#         return shipping

#     @property
#     def get_cart_total(self):
#         orderitems = self.orderitem_set.all()
#         total = sum(item.get_total() for item in orderitems)
#         return total 

#     @property
#     def get_cart_items(self):
#         orderitems = self.orderitem_set.all()
#         total = sum(item.quantity for item in orderitems)
#         return total 


# class OrderItem(models.Model):
#     product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
#     order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
#     quantity = models.IntegerField(default=0, null=True, blank=True)
#     date_added = models.DateTimeField(auto_now_add=True)

#     @property
#     def get_total(self):
#         total = self.product.price * self.quantity
#         return total


# class ShippingAddress(models.Model):
#     customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True)
#     order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
#     address = models.CharField(max_length=200, null=False)
#     city = models.CharField(max_length=200, null=False)
#     state = models.CharField(max_length=200, null=False)
#     zipcode = models.CharField(max_length=200, null=False)
#     date_added = models.DateTimeField(auto_now_add=True)

#     def __str__(self):
#         return self.address
    

  
# class Contact(models.Model):
#     name = models.CharField(max_length=255, null=False)
#     email = models.EmailField()
#     message = models.TextField()

#     def __str__(self):
#         return self.name
