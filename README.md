Coner Shop
Corner shop is an application developed to link the buyers and the sellers of goods to connect and purchase goods, and in the same sense, the ones who deliver ordered goods to reach out and fulfill the customers' expectations. 

#Functional requirements 
Business account.
    Sellers can create accounts with basic information (name, brief description etc )
    Sellers can upload logos, photos, among others.
Customer functionality 
    Buyers can place orders for online delivery, can leave reviews and ratings.
    Buyers can browse businesses by category,location,or keywords.
Admin panel
    Admin can manage user accounts (buyers and sellers ) and monitor platform activities.
    Admin can manage platform settings and configurations. 
Technical requirements 
    Backend: Django framework for server development 
    Database: MySQL for storing data
    Email API: Mailgun for order confirmations
    Cloud Hosting: AWS for web application
Development mehodology
    Extreme programming: Utilise XP practices to ensure agile development and high quality code.
    Testing:Implement unit, integration and user acceptance testing to ensure platform functonality and stability.
Authors
     Nuwarinda Albert
     Alinda Tracy
     Katushabe Annet
     Nabawanga Afuwa
     Tumwebaze Calvin

    

    


